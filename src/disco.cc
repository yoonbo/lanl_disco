#include "chroma.h"
#include "actions/ferm/invert/syssolver_linop_aggregate.h"
#include "meas/sources/z2_src.h"
#include "meas/sources/zN_src.h"
#include "meas/glue/mesfield.h"
#include "util/ft/sftmom.h"

#include "gamma_ops.h"  // definition of gamma_ops() ftn
#include <iomanip>      // std::setw
#include <algorithm>
#include <fstream>

// 1/sqrt(2)
#define INV_SQRT2 0.70710678118654752440

// Number of gamma operators (Ns * Ns = 16)
// If this is set to 17, the last one would be cEDM
#define NUM_G     16

//#define CALC_ERR_ERR

#define POW2(a) ((a) * (a))
#define POW3(a) ((a) * (a) * (a))
#define POW4(a) ((a) * (a) * (a) * (a))

using namespace std;
using namespace QDP;
using namespace Chroma;


// Anonymous namespace
namespace TSF
{
  // Standard Time Slicery
  class TimeSliceFunc : public SetFunc
  {
  public:
    TimeSliceFunc(int dir): dir_decay(dir) {}
    int operator() (const multi1d<int>& coordinate) const {return coordinate[dir_decay];}
    int numSubsets() const {return Layout::lattSize()[dir_decay];}
    int dir_decay;
  private:
    TimeSliceFunc() {}  // hide default constructor
  };
}

//====================================================================
// Structures for input parameters
//====================================================================

struct Inverter_t
{
  GroupXML_t  invParamHP;
  GroupXML_t  invParamLP;
  GroupXML_t  invParamLP_1;
  GroupXML_t  fermactLP;
  GroupXML_t  fermactHP;
  GroupXML_t  fermactHPE;
  Real        mass;
  bool        setupParamLP_1;
};

struct NoiseSource_t
{
  int version;

  int    Max_Nr_LP; // Minimum Nr_LP to terminate       
  int    Min_Nr_LP; // Minimum Nr_LP to terminate

  int    Nr_LPHP_ratio;

  bool          dilute;    // timeslice dilution
  bool          SC_dilute; // spin/color dilution
  multi1d<int>  timeslices;

  std::string   NoiseSrcType;
  
  // for Focus
  multi1d<multi1d<int>> src_coords;
  int                   radSq;
  
  // for Mesh 
  int                   mesh_size;

  // for version 2; restart if condition satisfied 
  int        restart_NrLP;  // Nr_LP to check if restart is needed
  double     restart_factor;// Restart criterion: err_Cr / err_LP > restart_factor
  GroupXML_t invParamLP_r;  // Restart LP loops with new inverter parameters 
};

struct Checkpoint_t
{
  int version;
  multi1d<double>     checkpoints;
  std::string         OutFileName;

  // below variables are for the program process, not for read
  std::vector<double> cp;
  int                 chkout_order;
};

struct Params_t
{
  multi1d<int>          nrow;
  Inverter_t            inverter;
  NoiseSource_t         noise_src;
  multi1d<Checkpoint_t> checkpoint;
  bool                  use_HPE;
  int                   mom2_max;
};

struct Inline_input_t
{
  Params_t        param;
  GroupXML_t      cfg;
  QDP::Seed       rng_seed;
};

//====================================================================
// Read input parameters
//====================================================================
void read(XMLReader& xml, const std::string& path, Inverter_t& p)
{
  XMLReader paramtop(xml, path);
  p.invParamHP = readXMLGroup(paramtop, "InvertParamHP", "invType");
  p.invParamLP = readXMLGroup(paramtop, "InvertParamLP", "invType");
  read(paramtop, "Mass", p.mass);

  // Fermion action for Low Precision Inverter
  if (paramtop.count("FermionActionLP") > 0) 
    p.fermactLP = readXMLGroup(paramtop, "FermionActionLP", "FermAct");
  else
    p.fermactLP = readXMLGroup(paramtop, "FermionAction", "FermAct");
 
  // Fermion action for Low Precision Inverter
  if (paramtop.count("FermionActionHP") > 0) 
    p.fermactHP = readXMLGroup(paramtop, "FermionActionHP", "FermAct");
  else
    p.fermactHP = readXMLGroup(paramtop, "FermionAction", "FermAct");

  // Fermion action for Hopping Parameter Expansion; must be unpreconditioned 
  p.fermactHPE = readXMLGroup(paramtop, "FermionActionHPE", "FermAct");

  // Inverter parameters for the first run of the inversions
  // For Multigrid inverter, the first inversion is setting up the inverter
  if (paramtop.count("InvertParamLP_1") > 0) {
    p.invParamLP_1 = readXMLGroup(paramtop, "InvertParamLP_1", "invType");
    p.setupParamLP_1 = true;
  }
  else {
    p.setupParamLP_1 = false;
  }
}

void read(XMLReader& xml, const std::string& path, NoiseSource_t& p)
{
  XMLReader paramtop(xml, path);

  // Read input style version 
  if (paramtop.count("version") > 0) {
    read(paramtop, "version", p.version);
  }
  else {
    p.version = 1; 
  }

  switch(p.version)
  {
    case 1:
      break;

    case 2:
      read(paramtop, "MaxNrLP",       p.Max_Nr_LP);
      read(paramtop, "MinNrLP",       p.Min_Nr_LP);
      read(paramtop, "RestartNrLP",   p.restart_NrLP);
      read(paramtop, "RestartFactor", p.restart_factor);
      p.invParamLP_r = readXMLGroup(paramtop, "InvertParamLP_r", "invType");
      break;

    default:
      QDPIO::cerr << "Error! NoiseSource input parameter version of " 
                  << p.version << " is not supported." << endl;
      QDP_abort(1);
  }

  read(paramtop, "NrLPHP_RATIO", p.Nr_LPHP_ratio);

  // Read Dilution information
  if (paramtop.count("Dilution") > 0) {
    p.dilute = true;
    read(paramtop, "Dilution/Timeslice", p.timeslices);
  }
  else {
    p.dilute = false;
  }

  // Read spin/color Dilution information
  if (paramtop.count("SpinColorDilution") > 0) {
    read(paramtop, "SpinColorDilution", p.SC_dilute);
  }
  else {
    p.SC_dilute = false;
  }

  // Read noise source type
  if (paramtop.count("NoiseSrcType") > 0) {
    read(paramtop, "NoiseSrcType", p.NoiseSrcType);
  }
  else {
    p.NoiseSrcType = "GAUSSIAN";
  }

  // Focus
  if (paramtop.count("Focus") > 0)
  {
    read(paramtop, "Focus/SrcCoords", p.src_coords);
    read(paramtop, "Focus/RadiusSq", p.radSq);
  }
  else
    p.src_coords.resize(0);

  // Mesh
  if (paramtop.count("Mesh") > 0)
    read(paramtop, "Mesh/size", p.mesh_size);
  else
    p.mesh_size   = 1;
}

void read(XMLReader& xml, const std::string& path, Checkpoint_t& p)
{
  XMLReader paramtop(xml, path);

  // Read input style version 
  if (paramtop.count("version") > 0) {
    read(paramtop, "version", p.version);
  }
  else {
    p.version = 1; 
  }

  read(paramtop, "Checkpoints", p.checkpoints);
  read(paramtop, "OutFile", p.OutFileName);
}

void read(XMLReader& xml, const std::string& path, Params_t& p)
{
  XMLReader paramtop(xml, path);
  read(paramtop, "nrow", p.nrow);

  if (paramtop.count("UseHPE") > 0)
    read(paramtop, "UseHPE", p.use_HPE);
  else
    p.use_HPE = true;

  if (paramtop.count("mom2_max") > 0)
    read(paramtop, "mom2_max", p.mom2_max);
  else
    p.mom2_max = 0;

  read(paramtop, "Inverter",    p.inverter);
  read(paramtop, "NoiseSource", p.noise_src);
  read(paramtop, "Checkpoint",  p.checkpoint);
}

void read(XMLReader& xml, const std::string& path, Inline_input_t& p)
{
  try {
    XMLReader paramtop(xml, path);

    read(paramtop, "Param",       p.param);
    p.cfg = readXMLGroup(paramtop, "Cfg", "cfg_type");
  
    if (paramtop.count("RNG") > 0)
      read(paramtop, "RNG", p.rng_seed);
    else
      p.rng_seed = 11;     // default seed 
  }
  catch( const std::string& e )
  {
    QDPIO::cerr << "Error reading XML : " << e << endl;
    QDP_abort(1);
  }
}

bool linkageHack(void)
{
  bool success = true;

  success &= GaugeInitEnv::registerAll();
  success &= WilsonTypeFermActsEnv::registerAll();
  success &= LinOpSysSolverEnv::registerAll();

  return success;
}

//====================================================================
// Structures for error analysis 
//====================================================================
struct ErrAnlyVars 
{
  int NumMom;

  multi3d<DComplex> TrM_inv_sum_LP; 
  multi3d<DComplex> TrM_inv_sum_C;
  multi3d<double>   TrM_inv_LP_sqsum_r;
  multi3d<double>   TrM_inv_LP_sqsum_i;
  multi3d<double>   TrM_inv_C_sqsum_r;
  multi3d<double>   TrM_inv_C_sqsum_i;

#ifdef CALC_ERR_ERR
  multi3d<vector<DComplex>> TrM_inv_est;
#endif

  ErrAnlyVars(){};

  ErrAnlyVars(int NumMom_, int NumTs)
  {
    NumMom = NumMom_;
    TrM_inv_sum_LP.resize(NUM_G, NumMom, NumTs); 
    TrM_inv_sum_C.resize(NUM_G, NumMom, NumTs);
    TrM_inv_LP_sqsum_r.resize(NUM_G, NumMom, NumTs);
    TrM_inv_LP_sqsum_i.resize(NUM_G, NumMom, NumTs);
    TrM_inv_C_sqsum_r.resize(NUM_G, NumMom, NumTs);
    TrM_inv_C_sqsum_i.resize(NUM_G, NumMom, NumTs);

#ifdef CALC_ERR_ERR
    TrM_inv_est.resize(NUM_G, NumMom, NumTs);
#endif

    for(int i=0; i<NUM_G; ++i) 
    for(int p=0; p<NumMom; ++p) 
    for(int t=0; t<NumTs; ++t){
      TrM_inv_sum_LP[i][p][t] = zero;
      TrM_inv_sum_C[i][p][t]  = zero;

      TrM_inv_LP_sqsum_r[i][p][t] = 0.0;
      TrM_inv_LP_sqsum_i[i][p][t] = 0.0;
      TrM_inv_C_sqsum_r[i][p][t]  = 0.0;
      TrM_inv_C_sqsum_i[i][p][t]  = 0.0;
    }
  }
};

//====================================================================
// Calculate statistical error of scalar channel
// and return maximum/average error among timeslices
//====================================================================
void check_acc(int Nr_LP, int Nr_HP, ErrAnlyVars &errAnly,
              vector<int> &timeslices, double &ratio_C_LP_err, 
              double &m_err_max, double &m_err_av,
              SftMom phases)
{
  int NumTs = timeslices.size();
  int g = 0; // scalar

  multi1d<int> momzero(Nd-1);
  momzero = 0;
  int p = phases.momToNum(momzero); // index for zero momentum

  // TSM estimate of Tr [ M^{-1} \gamma ]
  multi1d<DComplex> TrM_inv_av(NumTs);

  // Low precision estimate and correction to Tr [ G^{-1} \Gamma ]
  multi1d<DComplex> TrM_inv_av_LP(NumTs);
  multi1d<DComplex> TrM_inv_av_C(NumTs);

  // Calculate average
  if(Nr_LP != 0)
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_LP[t] = errAnly.TrM_inv_sum_LP[g][p][t]/(double)Nr_LP;
  else
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_LP[t] = zero;
    
  if(Nr_HP != 0)
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_C[t] = errAnly.TrM_inv_sum_C[g][p][t]/(double)Nr_HP;
  else
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_C[t] = zero;

  //-----------------------------
  // Calculate statistical error 
  //-----------------------------
  vector<double> TrM_inv_LP_err_r(NumTs);
  vector<double> TrM_inv_C_err_r(NumTs);
  vector<double> TrM_inv_err_r(NumTs);

  double TrM_inv_err_r_av = 0.0;

  for(int t=0; t<NumTs; ++t)
  {
    if(Nr_LP > 1) {
      double TrM_inv_av_LP_sq_r = pow((double)TrM_inv_av_LP[t].elem().elem().elem().real(), 2);
      TrM_inv_LP_err_r[t] = sqrt( (errAnly.TrM_inv_LP_sqsum_r[g][p][t]/Nr_LP - TrM_inv_av_LP_sq_r)/(Nr_LP-1.0) );
    } 
    else {
      TrM_inv_LP_err_r[t] = 0.0;
    }

    if(Nr_HP > 1) {
      double TrM_inv_av_C_sq_r = pow(TrM_inv_av_C[t].elem().elem().elem().real(), 2);
      TrM_inv_C_err_r[t] = sqrt( (errAnly.TrM_inv_C_sqsum_r[g][p][t]/Nr_HP - TrM_inv_av_C_sq_r)/(Nr_HP-1.0) );
    }
    else {
      TrM_inv_C_err_r[t] = 0.0;
    }

    TrM_inv_err_r[t] = sqrt( pow(TrM_inv_LP_err_r[t], 2) + pow(TrM_inv_C_err_r[t], 2) );
    TrM_inv_err_r_av += TrM_inv_err_r[t] / (double)NumTs;
  } // for(int t=0; t<NumTs; ++t)

  // Find maximum error among all timeslices; C++11 
  double max_LP_err = *std::max_element(TrM_inv_LP_err_r.begin(), TrM_inv_LP_err_r.end());
  double max_C_err  = *std::max_element(TrM_inv_C_err_r.begin(), TrM_inv_C_err_r.end());
  double max_err    = *std::max_element(TrM_inv_err_r.begin(), TrM_inv_err_r.end());

  // Calculate ratio between error of correction term and error of LP term;
  // this will be used for the restart criterion
  if(max_LP_err != 0)
    ratio_C_LP_err = max_C_err / max_LP_err;

  m_err_max = max_err;
  m_err_av  = TrM_inv_err_r_av;

  return;
}

//====================================================================
// Calculate statistical error, and save results 
//====================================================================
void checkout(int Nr_LP, int Nr_HP, ErrAnlyVars &errAnly, std::string out_fname, 
              vector<int> &timeslices, int chkout_order, bool Restarted,
              SftMom phases, bool SC_dilute)
{
  int NumTs = timeslices.size();
  int NumMom = errAnly.NumMom;

  if(NumMom != phases.numMom())
  {
    QDPIO::cerr << "Error! Inconsistent NumMom" << NumMom << " != " << phases.numMom()  << endl;
    QDP_abort(1);
  }


  // TSM estimate of Tr [ M^{-1} \gamma ]
  multi3d<DComplex> TrM_inv_av(NUM_G, NumMom, NumTs);

  // Low precision estimate and correction to Tr [ G^{-1} \Gamma ]
  multi3d<DComplex> TrM_inv_av_LP(NUM_G, NumMom, NumTs);
  multi3d<DComplex> TrM_inv_av_C(NUM_G, NumMom, NumTs);

  // Calculate average
  if(Nr_LP != 0)
    for(int g=0; g<NUM_G; ++g)
    for(int p=0; p<NumMom; ++p)
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_LP[g][p][t] = errAnly.TrM_inv_sum_LP[g][p][t]/(double)Nr_LP;
  else
    for(int g=0; g<NUM_G; ++g)
    for(int p=0; p<NumMom; ++p)
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_LP[g][p][t] = zero;

  if(Nr_HP != 0)
    for(int g=0; g<NUM_G; ++g)
    for(int p=0; p<NumMom; ++p)
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_C[g][p][t] = errAnly.TrM_inv_sum_C[g][p][t]/(double)Nr_HP;
  else
    for(int g=0; g<NUM_G; ++g)
    for(int p=0; p<NumMom; ++p)
    for(int t=0; t<NumTs; ++t)
      TrM_inv_av_C[g][p][t] = zero;


  //-----------------------------
  // Calculate statistical error 
  //-----------------------------
  multi3d<double> TrM_inv_LP_err_r(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_LP_err_i(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_C_err_r(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_C_err_i(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_err_r(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_err_i(NUM_G, NumMom, NumTs);

  for(int g=0; g<NUM_G; ++g)
  for(int p=0; p<NumMom; ++p)
  for(int t=0; t<NumTs; ++t)
  {
    if(Nr_LP > 1) {
      double TrM_inv_av_LP_sq_r = pow((double)TrM_inv_av_LP[g][p][t].elem().elem().elem().real(), 2);
      double TrM_inv_av_LP_sq_i = pow((double)TrM_inv_av_LP[g][p][t].elem().elem().elem().imag(), 2);
      if(SC_dilute)
      {
        TrM_inv_LP_err_r[g][p][t] = 0.0;
        TrM_inv_LP_err_i[g][p][t] = 0.0;
      }
      else
      {
        TrM_inv_LP_err_r[g][p][t] = sqrt( (errAnly.TrM_inv_LP_sqsum_r[g][p][t]/Nr_LP - TrM_inv_av_LP_sq_r)/(Nr_LP-1.0) );
        TrM_inv_LP_err_i[g][p][t] = sqrt( (errAnly.TrM_inv_LP_sqsum_i[g][p][t]/Nr_LP - TrM_inv_av_LP_sq_i)/(Nr_LP-1.0) );        
      }
    } 
    else {
      TrM_inv_LP_err_r[g][p][t] = 0.0;
      TrM_inv_LP_err_i[g][p][t] = 0.0;
    }

    if(Nr_HP > 1) {
      double TrM_inv_av_C_sq_r = pow(TrM_inv_av_C[g][p][t].elem().elem().elem().real(), 2);
      double TrM_inv_av_C_sq_i = pow(TrM_inv_av_C[g][p][t].elem().elem().elem().imag(), 2);
      if(SC_dilute)
      {
        TrM_inv_C_err_r[g][p][t] = 0.0;
        TrM_inv_C_err_i[g][p][t] = 0.0;
      }
      else
      {
        TrM_inv_C_err_r[g][p][t] = sqrt( (errAnly.TrM_inv_C_sqsum_r[g][p][t]/Nr_HP - TrM_inv_av_C_sq_r)/(Nr_HP-1.0) );
        TrM_inv_C_err_i[g][p][t] = sqrt( (errAnly.TrM_inv_C_sqsum_i[g][p][t]/Nr_HP - TrM_inv_av_C_sq_i)/(Nr_HP-1.0) );        
      }
    }
    else {
      TrM_inv_C_err_r[g][p][t] = 0.0;
      TrM_inv_C_err_i[g][p][t] = 0.0;
    }

    TrM_inv_err_r[g][p][t] = sqrt( pow(TrM_inv_LP_err_r[g][p][t], 2) + pow(TrM_inv_C_err_r[g][p][t], 2) );
    TrM_inv_err_i[g][p][t] = sqrt( pow(TrM_inv_LP_err_i[g][p][t], 2) + pow(TrM_inv_C_err_i[g][p][t], 2) );
  } // for(int g=0; g<NUM_G; ++g), for(int t=0; t<NumTs; ++t)


  //------------------------------------------
  // Print LP estimate and correction term (only p=0)
  //------------------------------------------
  if(chkout_order == -1) // -1 means final checkout
    QDPIO::cout << endl << "Checkout - fn; " << out_fname << endl;
  else
    QDPIO::cout << endl << "Checkout - " << chkout_order << "; " << out_fname << endl;

  QDPIO::cout << endl;

  multi1d<int> momzero(Nd-1);
  momzero = 0;
  int pzero = phases.momToNum(momzero); // index for zero momentum

  for(int t=0; t<NumTs; ++t){
    QDPIO::cout << "Timeslice = " << timeslices[t] << endl << endl;

    for(int g=0; g<NUM_G; ++g)
      QDPIO::cout << "Tr [ M^{-1} g" << g << " ]_LP = " << TrM_inv_av_LP[g][pzero][t]
        << " err( " << TrM_inv_LP_err_r[g][pzero][t] << ", " << TrM_inv_LP_err_i[g][pzero][t] << " )" << endl;

    QDPIO::cout << endl;

    for(int g=0; g<NUM_G; ++g)
      QDPIO::cout << "Tr [ M^{-1} g" << g << " ]_Cr = " << TrM_inv_av_C[g][pzero][t] 
        << " err( " << TrM_inv_C_err_r[g][pzero][t] << ", " << TrM_inv_C_err_i[g][pzero][t] << " )" << endl;

    QDPIO::cout << endl;
    QDPIO::cout << endl;
  } //for(int t=0; t<NumTs; ++t)

  //--------------------------------------------------------------------
  // Calculate TSM estimate of TrM_inv
  //--------------------------------------------------------------------
  for(int g=0; g<NUM_G; ++g)
  for(int p=0; p<NumMom; ++p)
  for(int t=0; t<NumTs; ++t)
    TrM_inv_av[g][p][t] = TrM_inv_av_LP[g][p][t] + TrM_inv_av_C[g][p][t];
 
  //--------------------------------------------------------------------
  // Calculate Another estimate of total error and error of error 
  //--------------------------------------------------------------------
#ifdef CALC_ERR_ERR
  multi3d<double> TrM_inv_est_av_r(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_est_av_i(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_est_err_r(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_est_err_i(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_est_err_err_r(NUM_G, NumMom, NumTs);
  multi3d<double> TrM_inv_est_err_err_i(NUM_G, NumMom, NumTs);
  
  for(int g=0; g<NUM_G; ++g)
  for(int p=0; p<NumMom; ++p)
  for(int t=0; t<NumTs; ++t){
    TrM_inv_est_av_r[g][p][t] = 0.0;
    TrM_inv_est_av_i[g][p][t] = 0.0;
    
    int N_e = errAnly.TrM_inv_est[g][p][t].size();
   
    // Calculate average
    double tmp_av_r = 0.0;
    double tmp_av_i = 0.0;
    for(int i=0; i<N_e; ++i) {
      tmp_av_r += errAnly.TrM_inv_est[g][p][t][i].elem().elem().elem().real();
      tmp_av_i += errAnly.TrM_inv_est[g][p][t][i].elem().elem().elem().imag();
    }
    tmp_av_r /= N_e;
    tmp_av_i /= N_e;

    TrM_inv_est_av_r[g][p][t] = tmp_av_r;
    TrM_inv_est_av_i[g][p][t] = tmp_av_i;

    // Calculate error and error of error
    double tmp_p2sum_r = 0.0;
    double tmp_p2sum_i = 0.0;
    double tmp_p4sum_r = 0.0;
    double tmp_p4sum_i = 0.0;

    for(int i=0; i<N_e; ++i) {
      double m_r = errAnly.TrM_inv_est[g][p][t][i].elem().elem().elem().real();
      double m_i = errAnly.TrM_inv_est[g][p][t][i].elem().elem().elem().imag();
      tmp_p2sum_r += POW2(m_r - tmp_av_r); 
      tmp_p2sum_i += POW2(m_i - tmp_av_i); 
      tmp_p4sum_r += POW4(m_r - tmp_av_r); 
      tmp_p4sum_i += POW4(m_i - tmp_av_i); 
    }

    // error
    double tmp_err_r = sqrt(tmp_p2sum_r / (N_e*(N_e-1.0)));
    double tmp_err_i = sqrt(tmp_p2sum_i / (N_e*(N_e-1.0)));

    TrM_inv_est_err_r[g][p][t] = tmp_err_r;
    TrM_inv_est_err_i[g][p][t] = tmp_err_i;

    // error of error
    double tmp_err_var_r = sqrt( tmp_p4sum_r/(N_e*POW3(N_e-1.0)) - POW4(tmp_err_r)/(N_e-1.0) );
    double tmp_err_var_i = sqrt( tmp_p4sum_i/(N_e*POW3(N_e-1.0)) - POW4(tmp_err_i)/(N_e-1.0) );
    TrM_inv_est_err_err_r[g][p][t] = tmp_err_var_r / (2*tmp_err_r);
    TrM_inv_est_err_err_i[g][p][t] = tmp_err_var_i / (2*tmp_err_i) ;
  }// Loop over g and t
#endif  // End of CALC_ERR_ERR


  //-----------------------------
  // Print results (only p=0)
  //-----------------------------
  for(int t=0; t<NumTs; ++t) {
    QDPIO::cout << "Timeslice = " << timeslices[t] << endl << endl;

    for(int g=0; g<NUM_G; ++g)
      QDPIO::cout << "Tr [ M^{-1} g" << g << " ]    = " << TrM_inv_av[g][pzero][t]
        << " std( " << TrM_inv_err_r[g][pzero][t] << ", " << TrM_inv_err_i[g][pzero][t] << " )"<< endl;

#ifdef CALC_ERR_ERR
    for(int g=0; g<NUM_G; ++g)
      QDPIO::cout << "Tr [ M^{-1} g" << g << " ]_est= ( " 
        << TrM_inv_est_av_r[g][pzero][t] << ", " << TrM_inv_est_av_i[g][pzero][t] << " ) "
        << " ( " << TrM_inv_est_err_r[g][pzero][t] << ", " << TrM_inv_est_err_i[g][pzero][t] << " )"
        << " ( " << TrM_inv_est_err_err_r[g][pzero][t] << ", " << TrM_inv_est_err_err_i[g][pzero][t] << " )"<< endl;
#endif
    QDPIO::cout << endl;
  } //for(int t=0; t<NumTs; ++t)


  //-----------------------------
  // Save results 
  //-----------------------------
  char buffer[250];
  char buffer_err[250];
  if(chkout_order == -1){ // -1 means that this checkout is the final
      sprintf(buffer, "%s_fn", out_fname.c_str());
      sprintf(buffer_err, "%s_err_fn", out_fname.c_str());
  }
  else{
      sprintf(buffer, "%s_%02d", out_fname.c_str(), chkout_order);
      sprintf(buffer_err, "%s_err_%02d", out_fname.c_str(), chkout_order);
  }

  std::string out_fname_c(buffer);
  std::string out_fname_err_c(buffer_err);

#ifdef CALC_ERR_ERR
  char buffer_err_err[250];
  if(chkout_order == -1) // -1 means that this checkout is the final
    sprintf(buffer_err_err, "%s_err_err_fn", out_fname.c_str());
  else
    sprintf(buffer_err_err, "%s_err_err_%02d", out_fname.c_str(), chkout_order);

  std::string out_fname_err_err_c(buffer_err_err);
#endif



//====================================================
// Save in binary format in order: 
// NumMom         : int, number of momenta in the list
// mom_list[p][i] : multi2d<int>, spatial momentum list
// NumTs          : int, number of time slices
// Num_G          : int, number of operators
// disco[idx][0,1]: multi2d<double>, disco result
//-------------------------------------------
// data array indices: 
// 0 <= p <= NumMom
// 0 <= i <= Nd-1 
// 0 <= idx <= NumMom*NumTs*NUM_G-1
//====================================================
#ifdef SAVE_BIN
  multi2d<int> mom_list(NumMom, Nd-1);
  // multi2d<double> disco(NumMom*NumTs*NUM_G,2);
  // multi2d<double> disco_err(NumMom*NumTs*NUM_G,2);
  multi3d<DComplex> disco(NumMom, NumTs, NUM_G);
  multi3d<DComplex> disco_err(NumMom, NumTs, NUM_G);
  int idx_ptg;


#ifdef CALC_ERR_ERR
  // multi2d<double> disco_err_err(NumMom*NumTs*NUM_G,2);
  multi3d<DComplex> disco_err_err(NumMom, NumTs, NUM_G);
#endif
  for(int p=0; p<NumMom; ++p) {
    multi1d<int> pp = phases.numToMom(p);
    mom_list[p][0]=pp[0];
    mom_list[p][1]=pp[1];
    mom_list[p][2]=pp[2];

    for(int t=0; t<NumTs; ++t) {
      // idx_ptg = p*NumTs*NUM_G + t*NUM_G;

      for(int g=0; g<NUM_G; ++g){
	// disco[idx_ptg+g][0]=TrM_inv_av[g][p][t].elem().elem().elem().real();
	// disco[idx_ptg+g][1]=TrM_inv_av[g][p][t].elem().elem().elem().imag();
	// disco_err[idx_ptg+g][0]=TrM_inv_err_r[g][p][t];
        // disco_err[idx_ptg+g][1]=TrM_inv_err_i[g][p][t];
	disco[p][t][g]=TrM_inv_av[g][p][t];
	disco_err[p][t][g].elem().elem().elem().real()=TrM_inv_err_r[g][p][t];
	disco_err[p][t][g].elem().elem().elem().imag()=TrM_inv_err_i[g][p][t];

#ifdef CALC_ERR_ERR
	disco_err_err[p][t][g].elem().elem().elem().real()=TrM_inv_est_err_err_r[g][p][t];
	disco_err_err[p][t][g].elem().elem().elem().imag()=TrM_inv_est_err_err_i[g][p][t]);
	// disco_err_err[idx_ptg+g][0]=TrM_inv_est_err_err_r[g][p][t];
	// disco_err_err[idx_ptg+g][1]=TrM_inv_est_err_err_i[g][p][t];
#endif
      } // for(int g=0; g<NUM_G; ++g)
    } // for(int t=0; t<NumTs; ++t)
  } // for(int p=0; p<NumMom; ++p)

  QDPIO::cout << "Saving disco in binary" << endl;
  BinaryFileWriter bin_out;
  bin_out.open(out_fname_c);

  write(bin_out, NumMom);
  write(bin_out, mom_list);
  write(bin_out, NumTs);
  write(bin_out, NUM_G);

  write(bin_out, disco);

  bin_out.close();

  QDPIO::cout << "Saving disco_err in binary" << endl;
  bin_out.open(out_fname_err_c);

  write(bin_out, NumMom);
  write(bin_out, mom_list);
  write(bin_out, NumTs);
  write(bin_out, NUM_G);

  write(bin_out, disco_err);

  bin_out.close();

#ifdef CALC_ERR_ERR
  QDPIO::cout << "Saving disco in binary" << endl;
  bin_out.open(out_fname_err_err_c);

  write(bin_out, NumMom);
  write(bin_out, mom_list);
  write(bin_out, NumTs);
  write(bin_out, NUM_G);

  write(bin_out, disco_err_err);

  bin_out.close();
#endif


#endif

//====================================================
// Save in text format
//====================================================
  TextFileWriter fout(out_fname_c+".txt");

  // Print header block
  {
    fout << "# NumMom NumT NumOps" << "\n" ;
    char buffer[250];
    sprintf(buffer, "%d %d %d\n", NumMom, NumTs, NUM_G);
    std::string oline(buffer);
    fout << oline;
  }

#ifdef CALC_ERR_ERR
    fout << "# px py pz  t   g  Tr[M^-1 g_i]_re  Tr[M^-1 g_i]_im   StatErr_re      StatErr_im       StatErrErr_re      StatErrErr_im" << "\n" ;
#else
    fout << "# px py pz  t   g  Tr[M^-1 g_i]_re  Tr[M^-1 g_i]_im   StatErr_re      StatErr_im" << "\n" ;
#endif
  
  for(int p=0; p<NumMom; ++p) {
    multi1d<int> pp = phases.numToMom(p);

    for(int t=0; t<NumTs; ++t) {

#ifdef CALC_ERR_ERR
      for(int g=0; g<NUM_G; ++g){
        char buffer[250];
        sprintf(buffer, "%2d %2d %2d %3d %2d %20.12e %20.12e %20.12e %20.12e %20.12e %20.12e\n",
          pp[0], pp[1], pp[2],
          timeslices[t],
          g,
          TrM_inv_av[g][p][t].elem().elem().elem().real(),
          TrM_inv_av[g][p][t].elem().elem().elem().imag(),
          TrM_inv_err_r[g][p][t],
          TrM_inv_err_i[g][p][t],
          TrM_inv_est_err_err_r[g][p][t],
          TrM_inv_est_err_err_i[g][p][t]);
        std::string oline(buffer);
        fout << oline;
      } // for(int g=0; g<NUM_G; ++g)
#else

      for(int g=0; g<NUM_G; ++g){
        char buffer[250];
        sprintf(buffer, "%2d %2d %2d %3d %2d %20.12e %20.12e %20.12e %20.12e\n",
          pp[0], pp[1], pp[2],
          timeslices[t],
          g,
          TrM_inv_av[g][p][t].elem().elem().elem().real(),
          TrM_inv_av[g][p][t].elem().elem().elem().imag(),
          TrM_inv_err_r[g][p][t],
          TrM_inv_err_i[g][p][t]);
        std::string oline(buffer);
        fout << oline;
      } // for(int g=0; g<NUM_G; ++g)
#endif
  
    } // for(int t=0; t<NumTs; ++t)
  } // for(int p=0; p<NumMom; ++p)
  
  fout.close();



  //-----------------------------
  // Save number of iterations
  //-----------------------------
  if(Layout::primaryNode())
  {
    // only the master node do the job
    char buffer[250];
    sprintf(buffer, "%s_iters", out_fname.c_str());
    std::string out_fname_c(buffer);

    std::ofstream fout;
    fout.open(out_fname_c, std::ios::out | std::ios::app);

    if(chkout_order == -1) // -1 means that this checkout is the final
      fout << " fn";
    else
      fout << " " << setfill('0') << setw(2) << chkout_order;

    fout << "  " << Nr_LP;

    if(Restarted)
      fout << "  R" << endl;
    else
      fout << endl;
  }
} // double checkout()


//===============================================
// Hopping parameter expansion (HPE)
//===============================================
// Let us write the clover operator M into
//   M = (1/2kappa) (1-kappa D)
// where D includes both the hopping term and the clover term.
// For clover action, x=y case, the trace of the M^-1 \Gamma is
//   Tr(M^-1 \Gamma) = Tr[ (2kappa*I + kappa^2 D^2 M^-1) \Gamma ]
// Except for the scalar case, the first term in Tr[] canceled
// so what we need to calculate is kappa^2 D^2 M^-1
// where D =(1/kappa -2M); kappa D = (1 - 2*kappa*M)
//
void do_HPE(Real kappa, LatticeFermion &psi, Handle< LinearOperator<LatticeFermion> > &M)
{
  Real mtwo_kappa = -2.0*kappa;

  LatticeFermion psi0, psi1;

  psi0 = zero;
  psi1 = psi;

  (*M)(psi0, psi, PLUS);  // psi = M psi
  psi0 *= mtwo_kappa;     // psi = -2*kappa*M psi
  psi  = psi1 + psi0;     // psi = (1 - 2*kappa*M)psi

  psi0 = zero;
  psi1 = psi;

  (*M)(psi0, psi, PLUS);  // psi = M psi
  psi0 *= mtwo_kappa;     // psi = -2*kappa*M psi
  psi  = psi1 + psi0;     // psi = (1 - 2*kappa*M)psi
}


//===================================
// Calculate cEDM term
//===================================

void calc_cEDM(multi1d<LatticeColorMatrix> &u, LatticePropagator &cEDM)
{
  assert(Nd == 4);
  
  multi1d<LatticeColorMatrix> f;
  mesField(f, u);

  // len(f) = Nd*(Nd-1)/2
  // f[0] = F_{01}
  // f[1] = F_{02}
  // f[2] = F_{03}
  // f[3] = F_{12}
  // f[4] = F_{13}
  // f[5] = F_{23}


  // Clover term looks
  //
  //   c_sw              * [sigma_01*F_01 + sigma_02*F_02 + ...]
  // + i * eps * gamma_5 * [sigma_01*F_01 + sigma_02*F_02 + ...]
  //
  // = [  sigma_01 * (c_sw*F_01 - i*eps*F_23)
  //    + sigma_02 * (c_sw*F_02 + i*eps*F_13)
  //    + ...]
  //
  // f0 = f[0] * getCloverCoeff(0,1) - timesI(f[5])*param.eps_cEDM;
  // f1 = f[1] * getCloverCoeff(0,2) + timesI(f[4])*param.eps_cEDM;
  // f2 = f[2] * getCloverCoeff(0,3) - timesI(f[3])*param.eps_cEDM;
  // f3 = f[3] * getCloverCoeff(1,2) - timesI(f[2])*param.eps_cEDM;
  // f4 = f[4] * getCloverCoeff(1,3) + timesI(f[1])*param.eps_cEDM;
  // f5 = f[5] * getCloverCoeff(2,3) - timesI(f[0])*param.eps_cEDM;


  // Note that
  // \sigma_{\mu\nu} = -i/2 [\gamma_\mu, \gamma_\nu]
  //                 = -i gamma_\mu \gamma_\nu  (for \mu \neq \nu)

  // cEDM term is
  //   i * eps * (1/2) * gamma_5 * sigma_{mu nu} * F_{mu nu} (sum over mu<nu)
  // = eps * (1/2) * gamma_5 * gamma_mu * gamma_nu F_{mu nu}
  // = eps * (1/2) *
  //  (- g0 g1 f[5]
  //   + g0 g2 f[4]
  //   - g0 g3 f[3]
  //   - g1 g2 f[2]
  //   + g1 g3 f[1]
  //   - g2 g3 f[0])
  //
  SpinMatrix ms = -1.0;
  multi1d<SpinMatrix> gammas(6);
  gammas[5] = ms * gamma_ops(13); //g0g1
  gammas[4] =      gamma_ops(15); //g0g2
  gammas[3] = ms * gamma_ops(10); //g0g3
  gammas[2] = ms * gamma_ops(14); //g1g2
  gammas[1] =      gamma_ops(11); //g1g3
  gammas[0] = ms * gamma_ops(12); //g2g3

  cEDM = zero;
  for(int i=0; i<6; ++i)
  {
    // Make identity propagator
    Complex cone = cmplx(Real(1),0);
    ColorMatrix tmp_cmat = zero;
    for (int color = 0; color < Nc; ++color)
      pokeColor(tmp_cmat, cone, color, color); // identity color matrix

    LatticePropagator tmp_prop = zero;
    for (int spin = 0; spin < Ns; ++spin)
      pokeSpin(tmp_prop, tmp_cmat, spin, spin);

    // Multiply gamma matrix with gauge field
    cEDM += f[i] * (gammas[i] * tmp_prop);
  } // for(int i=0; i<6; ++i)
}


//====================================================================
// Main program
//====================================================================
int main(int argc, char **argv)
{
  // Put the machine into a known state
  Chroma::initialize(&argc, &argv);
  
  // Put this in to enable profiling etc.
  START_CODE();

  QDPIO::cout << "Linkage = " << linkageHack() << endl;

  StopWatch snoop;
  snoop.reset();
  snoop.start();

  // Instantiate xml reader for DATA
  // if you used -i input file, Chroma::getXMLInputFileName() returns
  //  the filename you specified
  // otherwise it will return "DATA"
  XMLReader xml_in;
  Inline_input_t  input;

  try
  {
    xml_in.open(Chroma::getXMLInputFileName());
    read(xml_in, "/disco", input);
  }
  catch(const std::string& e)
  {
    QDPIO::cerr << "DISCO: Caught Exception reading XML: " << e << endl;
    QDP_abort(1);
  }
  catch(std::exception& e)
  {
    QDPIO::cerr << "DISCO: Caught standard library exception: " << e.what() << endl;
    QDP_abort(1);
  }
  catch(...)
  {
    QDPIO::cerr << "DISCO: caught generic exception reading XML" << endl;
    QDP_abort(1);
  }

  XMLFileWriter& xml_out = Chroma::getXMLOutputInstance();
  push(xml_out, "disco");

  // Write out the input
  write(xml_out, "Input", xml_in);

  Layout::setLattSize(input.param.nrow);
  Layout::create();

  proginfo(xml_out);    // Print out basic program info

  // Initialize the RNG
  QDP::RNG::setrn(input.rng_seed);
  write(xml_out,"RNG", input.rng_seed);

  // Initialize stop watch
  StopWatch swatch;

  // Start up the config
  swatch.reset();
  multi1d<LatticeColorMatrix> u(Nd);
  XMLReader gauge_file_xml, gauge_xml;

  // Start up the gauge field
  QDPIO::cout << "Attempt to read gauge field" << endl;
  swatch.start();
  try
  {
    std::istringstream  xml_c(input.cfg.xml);
    XMLReader  cfgtop(xml_c);
    QDPIO::cout << "Gauge initialization: cfg_type = " << input.cfg.id << endl;

    Handle< GaugeInit >
      gaugeInit(TheGaugeInitFactory::Instance().createObject(input.cfg.id,
                   cfgtop,
                   input.cfg.path));
    (*gaugeInit)(gauge_file_xml, gauge_xml, u);
  }
  catch(std::bad_cast)
  {
    QDPIO::cerr << "DISCO: caught cast error" << endl;
    QDP_abort(1);
  }
  catch(std::bad_alloc)
  {
    // This might happen on any node, so report it
    cerr << "DISCO: caught bad memory allocation" << endl;
    QDP_abort(1);
  }
  catch(const std::string& e)
  {
    QDPIO::cerr << "DISCO: Caught Exception: " << e << endl;
    QDP_abort(1);
  }
  catch(std::exception& e)
  {
    QDPIO::cerr << "DISCO: Caught standard library exception: " << e.what() << endl;
    QDP_abort(1);
  }
  catch(...)
  {
    // This might happen on any node, so report it
    cerr << "DISCO: caught generic exception during gaugeInit" << endl;
    QDP_abort(1);
  }
  swatch.stop();

  QDPIO::cout << "Gauge field successfully read: time= "
        << swatch.getTimeInSeconds()
        << " secs" << endl;

  XMLBufferWriter config_xml;
  config_xml << gauge_xml;

  // Write out the config header
  write(xml_out, "Config_info", gauge_xml);


  //====================================================================
  // Prepare variables
  //====================================================================
  Real kappa = massToKappa(input.param.inverter.mass);

  QDPIO::cout << "kappa = " <<  kappa << endl;

  QDPIO::cout << "Num src coords " << input.param.noise_src.src_coords.size() << endl;
  for(int i=0; i<input.param.noise_src.src_coords.size(); ++i)
  {
    for(int j=0; j<input.param.noise_src.src_coords[i].size(); ++j)
      QDPIO::cout << input.param.noise_src.src_coords[i][j] << " ";
    QDPIO::cout << endl;
  }

  QDPIO::cout << "Max momentum square = " << input.param.mom2_max << endl;

  bool use_HPE = input.param.use_HPE;
  if(use_HPE) QDPIO::cout << "Use Hopping Parameter Expansion"<< endl;

  bool setupParamLP_1 = input.param.inverter.setupParamLP_1;

  std::string NoiseSrcType = input.param.noise_src.NoiseSrcType;
  QDPIO::cout << "NoiseSrcType = " << NoiseSrcType << endl;

  // if noise_input_version == 1, do not use restart 
  // if noise_input_version == 2, restart the loop if std of correction is large
  int    noise_input_version = input.param.noise_src.version;
  int    Nr_LPHP_ratio = input.param.noise_src.Nr_LPHP_ratio;
  int    Max_Nr_LP     = input.param.noise_src.Max_Nr_LP;        
  int    Min_Nr_LP     = input.param.noise_src.Min_Nr_LP;    
  int    restart_NrLP  = input.param.noise_src.restart_NrLP; 
  double restart_factor= input.param.noise_src.restart_factor;

  bool dilute          = input.param.noise_src.dilute;
  bool SC_dilute       = input.param.noise_src.SC_dilute; // spin/color
  int  NumTs;
  vector<int> timeslices;
  if(dilute)
  {
    NumTs = input.param.noise_src.timeslices.size();
    for(int i=0; i<NumTs; ++i)
      timeslices.push_back(input.param.noise_src.timeslices[i]);
  }
  else
  {
    NumTs = Layout::lattSize()[3];
    for(int i=0; i<NumTs; ++i)
      timeslices.push_back(i);
  }

  if(dilute) {
    QDPIO::cout << "Calculate disconnected contribution on timeslice = ";
    for(int i=0; i < timeslices.size(); ++i)
      QDPIO::cout << timeslices[i] << " ";
    QDPIO::cout << endl;
  }

  if(SC_dilute){
    QDPIO::cout << "Spin / color dilution is used." << std::endl;
  }

  // Momentum for Fourier Transformation
  // mom2_max, avg_equiv_mom, j_decay
  SftMom phases(input.param.mom2_max, false, Nd-1);
  int NumMom = phases.numMom();

  multi1d<int> momzero(Nd-1);
  momzero = 0;
  int zerop = phases.momToNum(momzero); // index for zero momentum


  int num_checkp = input.param.checkpoint.size();
  std::vector<Checkpoint_t> checkp;
  for(int i=0; i<num_checkp; ++i) {
    checkp.push_back(input.param.checkpoint[i]);

    // Index of the order of the checkout; initialization
    checkp.back().chkout_order = 1;

    //sort checkpoints
    for(int j=0; j<checkp[i].checkpoints.size(); ++j)
      checkp[i].cp.push_back(checkp[i].checkpoints[j]);

    if(checkp[i].version == 1)
      // sort checkpoints in descending order
      std::sort(checkp[i].cp.begin(), checkp[i].cp.end(), std::greater<double>()); 
    else
      // sort checkpoints in ascending order
      std::sort(checkp[i].cp.begin(), checkp[i].cp.end()); 
  }

  // Noise source (eta) and Solution (psi)
  LatticeFermion eta, psi;
  LatticeComplex lat_cmplx_noise;    // for SC_dilute
  LatticeColorVector color_vec_srce; // for SC_dilute

  // Variables for error analysis
  ErrAnlyVars errAnly(NumMom, NumTs);

  // Temp variables
  Complex TrM_inv;
  LatticeFermion chi;
#ifdef CALC_ERR_ERR
  multi2d<DComplex> TrM_inv_est_LP_sum(NUM_G, NumMom, NumTs);
  for(int g=0; g<NUM_G; ++g) 
  for(int p=0; p<NumMom; ++p) 
  for(int t=0; t<NumTs; ++t)
    TrM_inv_est_LP_sum[g][p][t] = zero;
#endif

  LatticeComplex corr_fn;
  //multi1d<DComplex> corr_fn_t(Layout::lattSize()[Nd-1]);
  multi2d<DComplex> corr_fn_t;

  LatticeBoolean mask = false;

  //--------------------------
  // Calculate cEDM term
  //--------------------------
  LatticePropagator cEDM;
  calc_cEDM(u, cEDM);

  // Machinery to do timeslice sums with 
  Set TS;
  TS.make(TSF::TimeSliceFunc(Nd-1));

  //====================================================================
  // Do Inversion
  //====================================================================
  try{
    typedef LatticeFermion               T;
    typedef multi1d<LatticeColorMatrix>  P;

    //
    // Initialize fermion action
    //
    std::istringstream  xml_sLP(input.param.inverter.fermactLP.xml);
    XMLReader  fermacttopLP(xml_sLP);
    QDPIO::cout << "FermAct_LP = " << input.param.inverter.fermactLP.id << endl;

    std::istringstream  xml_sHP(input.param.inverter.fermactHP.xml);
    XMLReader  fermacttopHP(xml_sHP);
    QDPIO::cout << "FermAct_HP = " << input.param.inverter.fermactHP.id << endl;

    std::istringstream  xml_sHPE(input.param.inverter.fermactHPE.xml);
    XMLReader  fermacttopHPE(xml_sHPE);
    QDPIO::cout << "FermAct_HPE = " << input.param.inverter.fermactHPE.id << endl;

    // Generic Wilson-Type fermion action handles 
    Handle< WilsonTypeFermAct<T,P,P> >
      S_f_LP(TheWilsonTypeFermActFactory::Instance().createObject(input.param.inverter.fermactLP.id,
            fermacttopLP,
            input.param.inverter.fermactLP.path));

    Handle< WilsonTypeFermAct<T,P,P> >
      S_f_HP(TheWilsonTypeFermActFactory::Instance().createObject(input.param.inverter.fermactHP.id,
            fermacttopHP,
            input.param.inverter.fermactHP.path));

    Handle< WilsonTypeFermAct<T,P,P> >
      S_f_HPE(TheWilsonTypeFermActFactory::Instance().createObject(input.param.inverter.fermactHPE.id,
            fermacttopHPE,
            input.param.inverter.fermactHPE.path));

    Handle< FermState<T,P,P> > stateLP(S_f_LP->createState(u));
    Handle< FermState<T,P,P> > stateHP(S_f_HP->createState(u));
    Handle< FermState<T,P,P> > stateHPE(S_f_HPE->createState(u));

    // Solvers
    Handle< SystemSolver<LatticeFermion> > PP_LP;
    Handle< SystemSolver<LatticeFermion> > PP_LP_1;
    Handle< SystemSolver<LatticeFermion> > PP_HP;

    PP_LP   = S_f_LP->qprop(stateLP, input.param.inverter.invParamLP);
    PP_LP_1 = S_f_LP->qprop(stateLP, input.param.inverter.invParamLP_1);
    // PP_HP will be initialized in the loop

    // For the multigrid inverter, the only way to re-initialize the inverter
    // parameters is to call S->qprop(). Hence, in order to switch the
    // parameters  LP <--> HP, we need to call qprop() again.
    // The flag "HP_inv_called" tells us the HP inverter is called
    // and we need to re-initialize the LP parameters.
    bool HP_inv_called = false;

    // Boolean that indicates whether the loop is restarted
    bool Restarted = false;

    // Linear Operator for Hopping Parameter Expansion
    Handle< LinearOperator<T> > M(S_f_HPE->linOp(stateHPE));

    // Focus
    LatticeBoolean in_sites_f = false;
    if(input.param.noise_src.src_coords.size() > 0) {
      QDPIO::cout << "Use Focus on " << input.param.noise_src.src_coords.size()
        << " coordinates with radius^2 = " << input.param.noise_src.radSq << endl; 

      multi1d<int> L = Layout::lattSize();
      multi1d<int> x(Nd);

      double rad = sqrt(input.param.noise_src.radSq);

      for(int s_idx=0; s_idx<Layout::sitesOnNode(); ++s_idx)
      {
        multi1d<int> coord = Layout::siteCoords(Layout::nodeNumber(),s_idx);
        for(int foc=0; foc<input.param.noise_src.src_coords.size(); ++foc)
        {
          multi1d<int> x = input.param.noise_src.src_coords[foc];
          // do not need to calculate distance in those cases
          if(  min(abs(coord[0]-x[0]), L[0] - abs(coord[0]-x[0])) > rad
            || min(abs(coord[1]-x[1]), L[1] - abs(coord[1]-x[1])) > rad
            || min(abs(coord[2]-x[2]), L[2] - abs(coord[2]-x[2])) > rad )
            continue;

          double dist=0;
          for(int i=0; i<3; ++i)
            dist += pow(min(abs(coord[i]-x[i]), L[i] - abs(coord[i]-x[i])), 2);

          if(dist <= input.param.noise_src.radSq)
          {
            in_sites_f.elem(s_idx) = true; 
            break;
          }
        } // end of loop over foc
      } // end of loop over s_idx
    } // End of if Focus


    // Mesh
    // all possible offsets of meshes
    int num_mesh = pow(input.param.noise_src.mesh_size, 3);
    multi1d<LatticeBoolean> in_sites_m(num_mesh);
    if(input.param.noise_src.mesh_size > 1) {
      int mesh_size   = input.param.noise_src.mesh_size;
      QDPIO::cout << "Use mesh; size = " << mesh_size << endl;

      multi1d<int> mesh_offset(3);
      for(mesh_offset[0]=0; mesh_offset[0]<mesh_size; ++mesh_offset[0])
      for(mesh_offset[1]=0; mesh_offset[1]<mesh_size; ++mesh_offset[1])
      for(mesh_offset[2]=0; mesh_offset[2]<mesh_size; ++mesh_offset[2])
      {
        int i_mesh = mesh_offset[0] + mesh_offset[1]*mesh_size 
                   + mesh_offset[2]*mesh_size*mesh_size;
        in_sites_m[i_mesh] = false;

        for(int s_idx=0; s_idx<Layout::sitesOnNode(); ++s_idx)
        {
          multi1d<int> coord = Layout::siteCoords(Layout::nodeNumber(),s_idx);
          if( coord[0] % mesh_size == mesh_offset[0] 
           && coord[1] % mesh_size == mesh_offset[1] 
           && coord[2] % mesh_size == mesh_offset[2] )
          {
           in_sites_m[i_mesh].elem(s_idx) = true; 
          }
        } // end of loop over s_idx
      }
    } // End of if Mesh 

    //====================================================================
    // Truncated Solver
    //====================================================================
    // Here we use "Truncated Solver Method (TSM)" to estimate
    // inverse all-to-all propagator. It averages over NrLP
    // times of low-precision inversions, and corrects it by
    // using the NrHP times of high-precision inversions.

    //--------------------------------------------------------------------
    // Calculate estimation of propagator
    // M^-1 = (1 / Nr_LP) sum[ |psi_i>_LP <eta_i| ]
    // by using low precision calculation
    //--------------------------------------------------------------------

    int count_hp = 0;
    for(int count_lp=1; count_lp <= Max_Nr_LP; ++count_lp) {
      swatch.reset();
      swatch.start();

      QDPIO::cout << "TSM Low Precision Estimation loop; iter = " << count_lp << endl;
      
      // if use spin/color dilution, generate random noise source
      // of lattice complex and insert it to 12 spin/color components
      if(SC_dilute)
      {
        // Make noise source
        if(NoiseSrcType ==  "Z4") {
          int ZN = 4;

          LatticeReal rnd1, theta;
          random(rnd1);
          Real twopiN = Chroma::twopi / ZN;
          theta = twopiN * floor(ZN*rnd1);
          lat_cmplx_noise = cmplx(cos(theta),sin(theta));
        }
        else {
          QDPIO::cerr << "Error! Unknown noise source type " << NoiseSrcType << endl;
          QDPIO::cerr << "Allowed types are Z4." << endl;
          QDP_abort(1);
        }
      }
      else
      {
        // Make noise source
        if(NoiseSrcType == "Z2") {
          z2_src(eta);
        
          // gaussian() and z2_src require normalization factor (1/sqrt(2))
          eta *= INV_SQRT2;
        }
        else if(NoiseSrcType ==  "GAUSSIAN") {
          gaussian(eta);
  
          // gaussian() and z2_src require normalization factor (1/sqrt(2))
          eta *= INV_SQRT2;
        }
        else if(NoiseSrcType ==  "Z2REAL") {
          zN_src(eta, 2);
        }
        else if(NoiseSrcType ==  "Z4") {
          zN_src(eta, 4);
        }
        else {
          QDPIO::cerr << "Error! Unknown noise source type " << NoiseSrcType << endl;
          QDPIO::cerr << "Allowed types are Z2, Z2REAL, Z4 and GAUSSIAN." << endl;
          QDP_abort(1);
        }
      } // else of if(SC_dilute)

      // We will oop over spin/color indices, only if spin/color dilution is used
      int max_color_srcs = 1;
      int max_spin_srcs  = 1;
      if(SC_dilute)
      {
        max_color_srcs = Nc;
        max_spin_srcs  = Ns;
      }

      for(int color_source=0; color_source<max_color_srcs; color_source++)
      {
        if(SC_dilute)
        {
          color_vec_srce = zero;
          pokeColor(color_vec_srce, lat_cmplx_noise, color_source);
        }

        for(int spin_source=0; spin_source < max_spin_srcs; ++spin_source)
        {
          if(SC_dilute)
          {
            eta = zero;
            CvToFerm(color_vec_srce, eta, spin_source);
          }

        // Temp variables for AMA
        multi3d<DComplex> TrM_inv_C_HP(NUM_G, NumMom, NumTs);
        multi3d<DComplex> TrM_inv_C_LP(NUM_G, NumMom, NumTs);
        for(int g=0; g<NUM_G; ++g)
        for(int p=0; p<NumMom; ++p)
        for(int t=0; t<NumTs; ++t) {
          TrM_inv_C_HP[g][p][t] = zero;
          TrM_inv_C_LP[g][p][t] = zero;
        }


     
      // Calculate only on the timeslices
      if(dilute) {
        for(int t=0; t<NumTs; ++t)
          mask |= Layout::latticeCoordinate(3) == timeslices[t];
        eta = where(mask, eta, LatticeFermion(zero));
      }

      // Focus
      if(input.param.noise_src.src_coords.size() > 0) 
        eta = where(in_sites_f, eta, LatticeFermion(zero));

      // Mesh
      if(input.param.noise_src.mesh_size > 1) 
      {
        Real r;
        random(r);
        int rand_idx = (int)(toDouble(r)*num_mesh); // Random choice of mesh
        eta = where(in_sites_m[rand_idx], eta, LatticeFermion(zero));
      }

      psi = zero; // Initialize psi

      // Since qprop trashes the source, it should be copied safely
      chi = eta;

      // Low precision solver
      // This is called every iteration because this is the only way to 
      // (re)initialize the inverter parameters of multigrid solver
      if(HP_inv_called)
      {
        // if allocating new pointer to a Handle, it deletes old pointer
        if(Restarted)
          PP_LP = S_f_LP->qprop(stateLP, input.param.noise_src.invParamLP_r);
        else
          PP_LP = S_f_LP->qprop(stateLP, input.param.inverter.invParamLP);
        HP_inv_called = false;
      }

      // Calculate psi by using Dirac Inverter with Low Precision
      SystemSolverResults_t res;
      if(setupParamLP_1 && count_lp==1 && !Restarted) {
        // When the inverter runs at the first time, it may use different input
        // parameters, especially in the multigrid solver
        res = (*PP_LP_1)(psi, chi);
      }
      else {
        res = (*PP_LP)(psi, chi);
      }

      // Hopping parameter expansion (HPE)
      if(use_HPE)
        do_HPE(kappa, psi, M);

      // Calculate Tr(M^-1) = (1/N) sum_i <eta_i| \gamma |psi_i>
      for(int g=0; g<NUM_G; ++g) {
        switch(g)
        {
          case 16:
            corr_fn = localInnerProduct(eta, cEDM * psi);
            break;
          default:
            corr_fn = localInnerProduct(eta, gamma_ops(g) * psi);
        } // switch(g)
        //corr_fn_t = sumMulti(corr_fn, TS);
        corr_fn_t = phases.sft(corr_fn);

        // LP data for AMA correction term; will be discarded if 
        // this iteration is not for correction term
        // For the correction term, we don't need to add constant part Tr[2 kappa I]
        for(int p=0; p<NumMom; ++p)
        for(int t=0; t<NumTs; ++t)
          TrM_inv_C_LP[g][p][t] = corr_fn_t[p][timeslices[t]];

        for(int p=0; p<NumMom; ++p) {
          for(int t=0; t<NumTs; ++t) {
            TrM_inv = corr_fn_t[p][timeslices[t]];

            // For scalar case, HPE should correct Tr(2 kappa I) = 24*kappa*L^3
            if(g == 0 && p == zerop && use_HPE && color_source == max_color_srcs-1 && spin_source == max_spin_srcs-1)
              TrM_inv += 24.0*kappa*pow(Layout::lattSize()[0], 3);

            errAnly.TrM_inv_sum_LP[g][p][t] += TrM_inv;

            // Statistical error estimation
            errAnly.TrM_inv_LP_sqsum_r[g][p][t] += pow((double)TrM_inv.elem().elem().elem().real(), 2);
            errAnly.TrM_inv_LP_sqsum_i[g][p][t] += pow((double)TrM_inv.elem().elem().elem().imag(), 2);

#ifdef CALC_ERR_ERR
            TrM_inv_est_LP_sum[g][p][t] += TrM_inv;
#endif
          } // for(int t=0; t<NumTs; ++t)
        } // for(int p=0; p<NumMom; ++p)
      } // for(int g=0; g<NUM_G; ++g)

      // Print time used
      swatch.stop();

      QDPIO::cout << "TSM LP loop: time= "
            << swatch.getTimeInSeconds()
            << " secs" << endl;
      QDPIO::cout << endl;


      //--------------------------------------------------------------------
      // Calculate correction to the above low-precision estimation
      // Correction = (1 / Nr_HP) sum [ (|psi_j>_HP - |psi_j>_LP>) <eta_j| ]
      // by using LP and HP results
      // 
      // This part runs every Nr_LPHP_ratio times of LP iterations
      //--------------------------------------------------------------------
      if( Nr_LPHP_ratio > 0  &&  count_lp%Nr_LPHP_ratio == 0 ) {
        swatch.reset();
        swatch.start();

        QDPIO::cout << "TSM Correction Estimation Loop; iter = " << count_hp+1 << endl;
       
        //-----------------------------------------------------
        // High precision calculation
        //-----------------------------------------------------
        // Result (psi) of Low precision inversion is used as a
        // initial guess for this High precision calculation
        
        // Since qprop trashes the source, it should be copied safely
        chi = eta;

        // High precision solver
        // This is called every iteration because this is the only way to 
        // (re)initialize the inverter parameters of multigrid solver
        // if allocating new pointer to a Handle, it deletes old pointer
        PP_HP = S_f_HP->qprop(stateHP, input.param.inverter.invParamHP);
        HP_inv_called = true;

        // Calculate psi by using Dirac Inverter with High Precision
        SystemSolverResults_t res = (*PP_HP)(psi, chi);

        // Hopping parameter expansion (HPE)
        if(use_HPE)
          do_HPE(kappa, psi, M);

        // Calculate Tr(M^-1) = (1/N) sum_i <eta_i| \gamma |psi_i>
        for(int g =0; g<NUM_G; ++g) {
          switch(g)
          {
            case 16:
              corr_fn = localInnerProduct(eta, cEDM * psi);
              break;
            default:
              corr_fn = localInnerProduct(eta, gamma_ops(g) * psi);
          } // switch(g)
          //corr_fn_t = sumMulti(corr_fn, TS);
          corr_fn_t = phases.sft(corr_fn);

          // For the correction term, we don't need to add constant part Tr[2 kappa I]
          for(int p=0; p<NumMom; ++p)
          for(int t=0; t<NumTs; ++t)
            TrM_inv_C_HP[g][p][t] = corr_fn_t[p][timeslices[t]];
        }

        //-----------------------------------------------------
        // Calculate correction term
        //-----------------------------------------------------
        for(int g=0; g<NUM_G; ++g) 
        for(int p=0; p<NumMom; ++p)
        for(int t=0; t<NumTs; ++t){
          TrM_inv = TrM_inv_C_HP[g][p][t] - TrM_inv_C_LP[g][p][t];
          errAnly.TrM_inv_sum_C[g][p][t] += TrM_inv;

          // Statistical error estimation
          errAnly.TrM_inv_C_sqsum_r[g][p][t] += pow((double)TrM_inv.elem().elem().elem().real(), 2);
          errAnly.TrM_inv_C_sqsum_i[g][p][t] += pow((double)TrM_inv.elem().elem().elem().imag(), 2);

#ifdef CALC_ERR_ERR
          DComplex TrM_inv_est_tmp = TrM_inv_est_LP_sum[g][p][t]/Nr_LPHP_ratio + TrM_inv;
          errAnly.TrM_inv_est[g][p][t].push_back(TrM_inv_est_tmp);
          
          // Reinitialize
          TrM_inv_est_LP_sum[g][p][t] = zero;
#endif
        }

        // Print time used
        swatch.stop();

        QDPIO::cout << "TSM Correction loop (LP + HP): time= "
              << swatch.getTimeInSeconds()
              << " secs" << endl;
        QDPIO::cout << endl;

        // Increase number of hp iteration counter
        if(color_source == max_color_srcs-1 && spin_source == max_spin_srcs-1)
          ++count_hp;

      } // if( Nr_LPHP_ratio > 0  &&  i%Nr_LPHP_ratio == 0)

        } // for(spin_source)
      } // for(color_source)

      //------------------------------------------
      // Restart
      //------------------------------------------
      if(noise_input_version == 2 && !SC_dilute) {
        // Check whether restart is needed
        if(count_lp == restart_NrLP && !Restarted) {
          double ratio_C_LP_err, s_err_max, s_err_av;
          check_acc(count_lp, count_hp, errAnly, timeslices, ratio_C_LP_err, s_err_max, s_err_av, phases);

          // Restart the loop with new LP inverter parameters if the error of 
          // correction term is much larger than the error of LP term
          if(ratio_C_LP_err > restart_factor){
            QDPIO::cout << "Restart with new LP inverter parameters!" << endl;
            QDPIO::cout << "count_lp = " << count_lp 
                        << ", Cr_err / LP_err = " << ratio_C_LP_err
                        << ", Err_scalar = " << s_err_max << endl;

            count_lp     = 0; // for-loop will increase this by 1 in the end of this block
            count_hp     = 0;
            HP_inv_called= false;
            Restarted    = true;
            errAnly      = ErrAnlyVars(NumMom, NumTs);
            PP_LP        = S_f_LP->qprop(stateLP, input.param.noise_src.invParamLP_r);
            // Note that checkout order is not reinitialized; it will be proceed from where it is
          } // if(ratio_C_LP_err > restart_factor)
        } // if(count_lp == restart_NrLP && !Restarted)
      } // if(noise_input_version == 2)


      //------------------------------------------
      // Checkout or Finish 
      //------------------------------------------
      if(count_lp >= Min_Nr_LP) {
        // Calculate error of scalar channel (maximum value among timeslices)
        double ratio_C_LP_err, s_err_max, s_err_av;
        check_acc(count_lp, count_hp, errAnly, timeslices, ratio_C_LP_err, s_err_max, s_err_av, phases);

        QDPIO::cout << "count_lp = " << count_lp 
                    << ", Cr_err / LP_err = " << ratio_C_LP_err
                    << ", Err_scalar = " << s_err_max << endl;

        bool all_empty = true;

        // Loop over all types of checkpoints
        for(int idx_cp = 0; idx_cp < num_checkp; ++idx_cp) {
          while(1) {
            // break while(1) if checkpoints is empty
            if(checkp[idx_cp].cp.empty()) {
              all_empty &= true;
              break; 
            }

            double next_checkpoint = checkp[idx_cp].cp.back();
            bool chkout = false;

            // if this checkout is the last checkpoint, set it final (-1)
            if(checkp[idx_cp].cp.size() == 1)
              checkp[idx_cp].chkout_order = -1;

            // Determine whether checkout
            switch(checkp[idx_cp].version) {
              // Version 1: save results if the number of iterations reached to a checkpoint
              case 1:
                if(count_lp >= next_checkpoint) chkout = true;
                break;

              case 2:
                if(next_checkpoint > s_err_max) chkout = true;
                break;

              case 3:
                if(next_checkpoint > s_err_av) chkout = true;
                break;
            } // switch(checkp[idx_cp].version)

            // If it reaches to the Maximum iterations, checkout
            if(count_lp == Max_Nr_LP) {
              chkout = true;
              checkp[idx_cp].chkout_order = -1;
            }

            if(chkout) {
              // checkout
              checkout(count_lp, count_hp, errAnly, checkp[idx_cp].OutFileName, timeslices, checkp[idx_cp].chkout_order, Restarted, phases, SC_dilute);
              checkp[idx_cp].chkout_order++;

              // remove the passed checkpoint (the largest checkout accuracy)
              checkp[idx_cp].cp.pop_back(); 
            }
            // break while(1) if there is no checkpoints
            else {
              all_empty &= false; // if one of &= operates false, it will false
              break;
            }

            // break while(1) if checkpoints is empty
            if(checkp[idx_cp].cp.empty()) {
              all_empty &= true;
              break; 
            }
          } // end of while(1)

        } // for(int idx_cp = 0; idx_cp < num_checkp; ++idx_cp)


        if(all_empty) break;
      } // if(count_lp >= Min_Nr_LP)

    } // for(int count_lp=1; count_lp <= Max_Nr_LP; ++count_lp)

    xml_out.flush();
    pop(xml_out);

  } // try
  catch (const std::string &e){
    QDPIO::cerr << "Error in inversion: "<< e<<endl;
    QDP_abort(1);
  }

  //-----------------------------
  // End program 
  //-----------------------------
  snoop.stop();
  QDPIO::cout << "DISCO: total time = "
    << snoop.getTimeInSeconds()
    << " secs" << endl << flush;

  QDPIO::cout << "DISCO: ran successfully" << endl;

  END_CODE();

  Chroma::finalize();
  exit(0);
}
